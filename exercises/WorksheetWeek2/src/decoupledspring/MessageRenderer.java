package decoupledspring;

/**
 * Created by ohall03 on 20/01/2016.
 */
public interface MessageRenderer {
    public void render();

    public void setMessageProvider(MessageProvider provider);
    public MessageProvider getMessageProvider();
}
