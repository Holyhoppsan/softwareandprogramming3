package decoupledspring;

import java.io.FileInputStream;
import java.util.Properties;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.beans.factory.support.PropertiesBeanDefinitionReader;

/**
 * Created by ohall03 on 21/01/2016.
 */
public class HelloWorldSpring {

    public static void main(String[] args) throws Exception {

        //get the bean factory
        BeanFactory factory = getBeanFactory();

        MessageRenderer mr = (MessageRenderer) factory.getBean("renderer");
        MessageProvider mp = (MessageProvider) factory.getBean("provider");

        mr.setMessageProvider(mp);
        mr.render();
    }

    private static BeanFactory getBeanFactory() throws Exception {

        DefaultListableBeanFactory factory = new DefaultListableBeanFactory();

        //create a definition reader.
        PropertiesBeanDefinitionReader rdr = new PropertiesBeanDefinitionReader(factory);

        Properties props = new Properties();
        props.load(new FileInputStream("src/decoupledspring/bean.properties"));

        rdr.registerBeanDefinitions(props);

        return factory;

    }
}
