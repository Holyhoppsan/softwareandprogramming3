package decoupledspringautoscan;

/**
 * Created by ohall03 on 20/01/2016.
 */
public interface MessageProvider {
    public String getMessage();
}
