package decoupledspringautoscan;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by ohall03 on 21/01/2016.
 */
public class HelloWorldSpringWithAnnotation {

    public static void main(String[] args) throws Exception {

        //get the bean factory
        BeanFactory factory = getBeanFactory();

        MessageRenderer mr = (MessageRenderer) factory.getBean("renderer");
        mr.render();
    }

    private static BeanFactory getBeanFactory() throws Exception {
        System.out.println("Working Directory = " +
                System.getProperty("user.dir"));
        BeanFactory factory = new ClassPathXmlApplicationContext("file:src/decoupledspringautoscan/beans.xml");
        return factory;
    }
}
