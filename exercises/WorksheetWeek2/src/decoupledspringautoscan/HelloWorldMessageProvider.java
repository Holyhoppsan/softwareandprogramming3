package decoupledspringautoscan;

import org.springframework.stereotype.Component;

/**
 * Created by ohall03 on 20/01/2016.
 */

@Component
public class HelloWorldMessageProvider implements MessageProvider {

    public String getMessage(){
        return "Hello World! --- with Autoscan! How does that work?";
    }
}
