package decoupledspringautoscan;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by ohall03 on 20/01/2016.
 */
@Component("renderer")
public class StandardOutMessageRenderer implements MessageRenderer {

    @Autowired
    private MessageProvider messageProvider = null;

    public void render(){
        if(messageProvider == null) {
            throw new RuntimeException("You must set the property messageProvider of class: "
                        + StandardOutMessageRenderer.class.getName());
        }

        System.out.println(messageProvider.getMessage());
    }
}
