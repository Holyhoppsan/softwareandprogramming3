package decoupledspringdixml;

/**
 * Created by ohall03 on 20/01/2016.
 */
public class HelloWorldMessageProvider implements MessageProvider {
    public String getMessage(){
        return "Hello World!";
    }
}
