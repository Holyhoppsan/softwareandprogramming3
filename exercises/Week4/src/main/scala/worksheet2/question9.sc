//9. When entered on the REPL, what does the following program output, and what is
//the type and value of the final expression?
//object argh {
//  def a = {
//    println("a")
//  }
//  val b = {
//    println("b")
//    a + 2
//  }
//  def c = {
//    println("c")
//    a
//    b + "c"
//  }
//}
// argh.c + argh.b + argh.a
//Think carefully about the types, dependencies, and evaluation behaviour of each field
//  and method.

object argh {
  def a = {
    println("a")
  }
  val b = {
    println("b")
    a + 2
  }
  def c = {
    println("c")
    a
    b + "c"
  }
}

 argh.c + argh.b + argh.a

//a is being evaluated to a string
//b does not compile as it tries to combine a string with an int
//c should compile if the statement a+2 is removed. that would return
//the concatenation between b and "c"