// 1. What are the values and types of the following Scala literals?
//(a) 42
42
//Answer: it is an in with the value 42

//(b) true
true
// Answer: it is of type Boolean and the value true

//(c) 123L
123L
// Answer: it is of type Long with the value 123

//(d) 42.0
42.0
//Answer: it is of type Double and of value 42.0
