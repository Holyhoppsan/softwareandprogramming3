//7.
// Define an object called calc with a method square that accepts a Double as an
// argument and…you guessed it…squares its input. Add a method called cube that
// cubes its input and calls square as part of its result calculation.

object calc {
  def square(x : Double) : Double = x*x
  def cube(x : Double) : Double = square(x) * x
}