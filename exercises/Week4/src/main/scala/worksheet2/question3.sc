//3. What is the difference between the following expressions?
//(a) "Hello world”!
"Hello world"

//(b) println("Hello world”)!
println("Hello world")

//What is the type and value of each?
//"Hello World" returns a instance of type string
//while println return the type Unit(equivalent of void in c++)