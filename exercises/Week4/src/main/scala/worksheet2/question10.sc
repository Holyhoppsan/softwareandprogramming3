//10.
// (a) Define an object called Person that contains fields called firstName and
// lastName.
// (b)
// Define a second object called Alien containing a method called greet that takes
// your Person as a parameter and returns a greeting using their firstName.
// What is the type of the greet method? Can we use this method to greet other
// objects?

class Person(firstName:String, lastName:String) {

}

class Alien {
  def greet(person : Person) {
    val greeting = s"Greetings {person.firstName}"
  }
}

val test = new Person("daniel", "hall")
val alien = new Alien()

alien.greet(test)
