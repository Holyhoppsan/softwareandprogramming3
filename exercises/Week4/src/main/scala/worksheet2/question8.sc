//8.
// Copy and paste calc from the previous exercise to create a calc2 that is generalised
// to work with Ints as well as Doubles. As you have Java experience, this should be
// fairly straightforward.

class calc2 {
  def square[A:Numeric](a:A) = {
    implicitly[Numeric[A]].times(a, a)
  }
  def cube[A:Numeric](x : A) : A = implicitly[Numeric[A]].times(square(x), x)
}

val c = new calc2()

c.square(2)
c.cube(2)

c.square(3.4)