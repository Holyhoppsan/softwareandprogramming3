// 5.
// What are the types and values of the following conditionals?
//(a)
val a = 1
val b = 2
if(a > b) "alien" else "predator"
//The conditional here returns a string

//(b)
val c = 1
val d = 2
if(c > d) "alien" else 2001
//the conditional returns an Any as that is the most common base class.

//(c)
if(true) "hello"
// theres an implicit else case making the resulting value to become
// uncertain and hence Any