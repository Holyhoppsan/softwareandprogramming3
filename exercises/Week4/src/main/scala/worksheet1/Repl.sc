//1)
val identifier = 17

//2) This fails as values are immutable in scala
//identifier = 20

//3)
var testString = "ABC1234"

//4) This, unlike java and c#, compiles fine as strings are not
// immutable in scala as they are in java
testString = "DEF1234"

//5) It deduces the type to a double for the value and prints
val testDouble = 15.56