//1) the difference between the ranges is that apart from
// the step value is that the ranges have different number of elements
//withing them where the second range only contains
//the odd values because it started at 1 in this case.
//In the first range the range gets a different type, namely Inclusive
val range = 1 to 10
println(range.step)
val range2 = 1 to 10 by 2
println(range2.step)
//2)
var s1 = "Sally"

var s2 = "Sally"

if (s1.equals(s2)){
  println(s1)
}
else {
  println(s2)
}
