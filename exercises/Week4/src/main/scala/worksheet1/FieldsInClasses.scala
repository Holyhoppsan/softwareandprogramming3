import AtomicTest._


class Cup {
  var percentFull = 0
  val max = 100
  def add(increase:Int):Int = {
    percentFull += increase
    if(percentFull > max) {
      percentFull = max
    }
    if(percentFull < 0){
      percentFull = 0
    }

    percentFull // Return this value
  }

  def set(value: Int) : Unit = {
    percentFull = value
  }

  def get() : Int = {
    percentFull
  }
}

object myApp extends App {
  // 1)
  val cup = new Cup
  cup.add(45) is 45
  cup.add(-15) is 30
  //cup.add(-50) is -20
  cup.add(-50) is 0

  // 2)
  val cup2 = new Cup
  cup2.add(45) is 45
  cup2.add(-55) is 0
  cup2.add(10) is 10
  cup2.add(-9) is 1
  cup2.add(-2) is 0

  // 3)
  // Yes this is possible as members without
  // an access modifier are public by default.
  cup.percentFull = 56
  cup.percentFull is 56

}


//4)