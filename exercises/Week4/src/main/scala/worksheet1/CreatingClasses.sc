//1)
class Hippo()
val testHippo = new Hippo()
print(testHippo)

class Lion()
val testLion = new Lion()
print(testLion)

class Tiger()
val testTiger = new Tiger()
print(testTiger)

class Monkey()
val testMonkey = new Monkey()
print(testMonkey)

class Giraffe()
val testGiraffe = new Giraffe()
print(testGiraffe)

// 2) printing these instances will give different
// addresses to them since they are stored in different
// places in memory
val secondLion = new Lion()
print(secondLion)

val secondGiraffe = new Giraffe()
print(secondGiraffe)