//1)
val Sky = "sunny"
val Temperature = 80.0

var expression = if(Sky == "sunny" && Temperature == 80.0) {
  true
}
else {
  false
}

//2)

var expression2 = if((Sky == "sunny" || Sky == "partly cloudy")
                  && (Temperature > 80.0)) {
  true
}
else {
  false
}

//3)

var expression3 = if((Sky == "sunny" || Sky == "partly cloudy")
                  && (Temperature > 80.0 || Temperature < 20.0)) {
  true
}
else {
  false
}

//4)

def FahrenheitToCelsius(tempInFahrenheit : Double)
  : Double = (tempInFahrenheit - 32.0) *(5.0/9.0)

val TemperatureInCelsius = FahrenheitToCelsius(Temperature)

//5)
def CelsiusToFahrenheit(tempInCelsius : Double)
  : Double = (tempInCelsius*(9.0/5.0)) + 32.0

val TemperatureInFahrenheit = CelsiusToFahrenheit(TemperatureInCelsius)