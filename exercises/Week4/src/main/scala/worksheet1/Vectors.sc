// 1)

val vec1 = Vector(1, 3,4, 5)
val vec2 = Vector(1.993, 5.678)
val vec3 = Vector("fasd", "fasdf","sdfsa")

// 2)

val vec4 = Vector( Vector(2,3,4), Vector(5,6,7))

val vec5 = Vector.fill(3, 3)( 0 )

// 3)

val sentence = Vector("The", "dog", "visited", "the", "fire", "station")

for(word <- sentence)
{
  print(word + " ")
}

//sentence.toString() is "The dog visited the fire station "

// 4)

val vec6 = Vector(1,2,3,4,5,6)

vec6.sum
vec6.min
vec6.max

val vec7 = Vector(2.34,454.6,23.44)

vec7.sum
vec7.min
vec7.max

// 5)

val myVector1 = Vector(1,2,3,4,5,6)
val myVector2 = Vector(1,2,3,4,5,6)

for(i <- 0 until myVector1.length; j <- 0 until myVector2.length)
{
  // i is j
}