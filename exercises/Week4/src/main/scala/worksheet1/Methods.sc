//1)

def getSquare(x : Int) : Int = x*x

val a = getSquare(3)
assert(a == 9)

val b = getSquare(6)
assert(b == 36)

val c = getSquare(5)
assert(c == 25)


//2)

def isArg1GreaterThanArg2(arg1 : Double, arg2 : Double) : Boolean = arg1 > arg2

val t1 = isArg1GreaterThanArg2(4.1, 4.12)
assert(t1 == false)
val t2 = isArg1GreaterThanArg2(2.1, 1.2)
assert(t2 == true)

//3)

def manyTimesString(inputString : String, multiplier : Int)  : String = inputString * multiplier

val m1 = manyTimesString("abc", 3)
assert("abcabcabc" == m1, "the function manyTimesString could not repeat the string three times")
val m2 = manyTimesString("123", 2)
assert("123123" == m2, "the function manyTimesString could not repeat the string two times")

